(function ($,ng) {

    'use strict';

    var app = ng.module('app',['ngSanitize','mdr.select2']);
    //config
    app.config(['$httpProvider','$compileProvider','$sceDelegateProvider',function($httpProvider, $compileProvider,$sceDelegateProvider) {
        // $compileProvider.imgSrcSanitizationWhitelist(/^\s*(blob):/);
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);

        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self',
            // Allow loading from our assets domain.  Notice the difference between * and **.
            'http://vk.com/**'
        ]);


        $httpProvider.interceptors.push(['$q', '$rootScope',function($q, $rootScope) {
            return {
                'request': function(config) {
                    $rootScope.$broadcast('loading-started');
                    return config || $q.when(config);
                },
                'response': function(response) {
                    $rootScope.$broadcast('loading-complete');
                    return response || $q.when(response);
                }
            };
        }]);
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

        // Переопределяем дефолтный transformRequest в $http-сервисе
        $httpProvider.defaults.transformRequest = [function(data)
        {
            /**
             * рабочая лошадка; преобразует объект в x-www-form-urlencoded строку.
             * @param {Object} obj
             * @return {String}
             */
            var param = function(obj)
            {
                var query = '';
                var name, value, fullSubName, subValue, innerObj, i;

                for(name in obj)
                {
                    value = obj[name];

                    if(value instanceof Array)
                    {
                        for(i=0; i<value.length; ++i)
                        {
                            subValue = value[i];
                            fullSubName = name + '[' + i + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    }
                    else if(value instanceof Object)
                    {
                        for(var subName in value)
                        {
                            subValue = value[subName];
                            fullSubName = name + '[' + subName + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    }
                    else if(value !== undefined && value !== null)
                    {
                        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                    }
                }

                return query.length ? query.substr(0, query.length - 1) : query;
            };

            return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];
    }]);




    var Shares = {
        /*title: 'Поделиться',*/
        width: 600,
        height: 400,

        /*init: function() {
            var share = document.querySelectorAll('.social');
            for(var i = 0, l = share.length; i < l; i++) {
                var url = share[i].getAttribute('data-url') || location.href, title = share[i].getAttribute('data-title') || '',
                    desc = share[i].getAttribute('data-desc') || '', el = share[i].querySelectorAll('a');
                for(var a = 0, al = el.length; a < al; a++) {
                    var id = el[a].getAttribute('data-id');
                    if(id)
                        this.addEventListener(el[a], 'click', {id: id, url: url, title: title, desc: desc});
                }
            }
        },

        addEventListener: function(el, eventName, opt) {
            var _this = this, handler = function() {
                _this.share(opt.id, opt.url, opt.title, opt.desc);
            };
            if(el.addEventListener) {
                el.addEventListener(eventName, handler);
            } else {
                el.attachEvent('on' + eventName, function() {
                    handler.call(el);
                });
            }
        },*/

        share: function(id, url, title, desc) {
            url = encodeURIComponent(url);
            desc = encodeURIComponent(desc);
            title = encodeURIComponent(title);
            switch(id) {
                case 'fb':
                    this.popupCenter('https://www.facebook.com/sharer/sharer.php?u=' + url, title, this.width, this.height);
                    break;
                case 'vk':
                    this.popupCenter('https://vk.com/share.php?url=' + url, title, this.width, this.height);
                    break;
                case 'tw':
                    var text = title || desc || '';
                    if(title.length > 0 && desc.length > 0)
                        text = title + ' - ' + desc;
                    if(text.length > 0)
                        text = '&text=' + text;
                    this.popupCenter('https://twitter.com/intent/tweet?url=' + url + text, title, this.width, this.height);
                    break;
                case 'gp':
                    this.popupCenter('https://plus.google.com/share?url=' + url, title, this.width, this.height);
                    break;
                case 'ok':
                    this.popupCenter('https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=' + url, title, this.width, this.height);
                    break;
            }
        },

        newTab: function(url) {
            var win = window.open(url, '_blank');
            win.focus();
        },

        popupCenter: function(url, title, w, h) {
            var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;
            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
            var left = ((width / 2) - (w / 2)) + dualScreenLeft;
            var top = ((height / 3) - (h / 3)) + dualScreenTop;
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
            if (window.focus) {
                newWindow.focus();
            }
        }
    };

    $('.social a').on('click', function() {
        var id = $(this).data('id');
        console.log($(this).parent()[0]);
        if(id) {
            var data = $(this).parent('.social');
            var url = data.data('url') || location.href, title = data.data('title') || '', desc = data.data('desc') || '';

            console.log(id, url, title, desc);
            Shares.share(id, url, title, desc);
        }
    });


    $('.smooth-scroll').click(function () {
        $(window).scrollTo($(this).attr('href'),500);

    });





    app.controller('main',['$scope', '$timeout', function($scope, $timeout) {

        $scope.social = function (id){
            var url = $scope.present.repost || location.href, title = $scope.present.title || '', desc = $scope.present.text || '';

            Shares.share(id, url, title, desc);
        };

        $scope.phrases_woman = [
            {
                key: 'Я хочу в подарок сертификат в спа или салон красоты',
                value: 'Нужно сказать: "Женщины созданы для красоты и любви."'
            },
            {
                key: 'Ну где же цветы?',
                value: 'Нужно сказать: "Ты не забыл, какой сегодня день?"'
            },
            {
                key: 'Я хочу, эту дорогущую сумочку',
                value: 'Нужно сказать: "Ты меня любишь?"'
            },
            {
                key: 'Ты обязан согласиться',
                value: 'Нужно сказать:"Ты согласен?"'
            },
            {
                key: 'Я хочу пойти на девичник на всю ночь',
                value: 'Нужно сказать: "Мы с девочками недолго посидим"'
            },
            {
                key: 'Если у вас не хватает аргументов в споре',
                value: 'Нужно сказать: "Ой, все!"'
            },
            {
                key: 'Я хочу узнать, что за девушка у него в друзьях в Фейсбуке',
                value: 'Нужно сказать: "А красивая у тебя подружка"'
            },
            {
                key: 'Если вдруг он смотрит по сторонам и не обращает внимания на ваше новое платье',
                value: 'Говорите: "Я не испачкалась?"'
            },
            {
                key: 'Хотите, чтобы он взял за руку, а лучше на руки',
                value: 'Говорите: "Тут так скользко!"'
            },
            {
                key: 'Я хочу отправиться на шопинг',
                value: 'Нужно сказать: "Мне совершенно нечего надеть!"'
            }
        ];
        $scope.phrases_man = [
            {
                key: 'Нам нужно',
                value: 'Означает: "Я хочу!"'
            },
            {
                key: 'Эта сумочка подошла бы к цвету моего лака на ногтях',
                value: 'Означает: "Мне нужна эта сумочка и новый лак."'
            },
            {
                key: 'Выбери сам, куда пойдем',
                value: 'Означает: "Выбери мой любимый ресторан."'
            },
            {
                key: 'Может, пойдем, погуляем?',
                value: 'Означает: "Пошли скорей по магазинам!"'
            },
            {
                key: 'Я буду готова через минуту',
                value: 'Означает: "Я буду готова минимум через полчаса."'
            },
            {
                key: 'Я хочу новые туфли',
                value: 'Означает: "И платье, и украшения, сумочку."'
            },
            {
                key: 'Выбери сам',
                value: 'Означает: "Надеюсь, ты выберешь то, что подороже."'
            },
            {
                key: 'Ты согласен? ',
                value: 'Означает: "Ты обязан согласиться."'
            },
            {
                key: 'Я не расстроена  ',
                value: 'Означает: "Я очень расстроена."'
            },
            {
                key: 'Мне ничего не нужно',
                value: 'Означает: "Ты прекрасно знаешь, что я хочу, и попробуй только не подарить этого!"'
            }
        ];

        $scope.phrases_social = function (id){
            var url = location.href, title = $scope.man_active_phrase.key || '', desc = $scope.man_active_phrase.value || '';

            Shares.share(id, url, title, desc);
        };
        $scope.man_active_phrase = '';
        $scope.showphrasepopup = false;
        $scope.$watch('man_active_phrase',function(new_val, old_val){
            if (new_val && new_val.value) {
                $timeout(function(){
                    $scope.showphrasepopup = true;

                });
            }
        });



        $scope.aud = [

            {
                id: 1,
                name: 'Любимый',
                checked: false
            },
            {
                id: 2,
                name: 'Папа',
                checked: false
            },
            {
                id: 3,
                name: 'Брат',
                checked: false
            },
            {
                id: 4,
                name: 'Сын',
                checked: false
            },
            {
                id: 5,
                name: 'Коллега/Друг',
                checked: false
            }
        ];
        $scope.aud_man = [

            {
                id: 1,
                name: 'Вторая половинка',
                checked: false
            },
            {
                id: 2,
                name: 'Мама',
                checked: false
            },
            {
                id: 3,
                name: 'Дочь',
                checked: false
            },
            {
                id: 4,
                name: 'Сестра',
                checked: false
            },
            {
                id: 6,
                name: 'Бабушка',
                checked: false
            },
            {
                id: 5,
                name: 'Коллега/ Подруга',
                checked: false
            }
        ];
        $scope.ages =  [
            {
                id: 6,
                name: '0-10',
                checked: false
            },
            {
                id: 7,
                name: '10-15',
                checked: false
            },
            {
                id: 1,
                name: '16-24',
                checked: false
            },
            {
                id: 2,
                name: '25-34',
                checked: false
            },
            {
                id: 3,
                name: '35-44',
                checked: false
            },
            {
                id: 4,
                name: '45-54',
                checked: false
            },
            {
                id: 5,
                name: '55+',
                checked: false
            }
        ];


        $scope.aud_arr = [
            '',
            'Любимый',
            'Папа',
            'Брат',
            'Сын',
            'Друг'
        ];


        $scope.woman_activeaud = 0;
        $scope.activeage = 0;

        $(document).keydown(function(e) {
            if( e.keyCode === 27 ) {
                $timeout(function () {
                    $scope.showphrasepopup = false;
                    $scope.openpresent = false;
                });
            }
        });


        $scope.present = {
            title: '',
            link: '',
            picture: '',
            text: ''
        };

        $scope.openpresent = false;

        $scope.man_generator = function () {
            $scope.present = {
                title: false,
                link: '',
                picture: '',
                repost: '',
                text: 'Пожалуйста, выберите другой возраст'
            };
            switch ($scope.woman_activeaud) {
                case 1: //Вторая половинка
                    switch ($scope.activeage) {

                        case 1://16-24
                            $scope.present = {
                                title: 'Смарт-часы Apple Watch Sport 42mm with Sport Band Black',
                                link: 'https://goo.gl/7wKG8V',
                                picture: '/img/gifts/apple-watch.jpg',
                                repost: 'http://womansday.nur.kz/pages/apple-watch-sport.html',
                                text: 'Спортивной леди, особенно, с подаренным вами айфоном, просто необходимо иметь спортивные часы.'
                            };
                            break;
                        case 2:
                            $scope.present = {
                                title: 'Смартфон Apple iPhone 7 32Gb Black',
                                link: 'https://goo.gl/nzlZlQ',
                                picture: '/img/gifts/apple-iphone.jpg',
                                repost: 'http://womansday.nur.kz/pages/iphone-7.html',
                                text: 'Покажите ту девушку, которая не будет рада седьмому айфону. Таких не существует в природе. Идеальный подарок!'
                            };
                            break;
                        case 3:
                            $scope.present = {
                                title: 'Планшет Apple iPad Air 2 16Gb Wi-Fi Space Gray',
                                link: 'https://goo.gl/sFJkR1',
                                picture: '/img/gifts/apple-ipad-air.jpg',
                                repost: 'http://womansday.nur.kz/pages/ipad-air.html',
                                text: 'Продукция Apple вне конкуренции, вторая половинка это оценит. '
                            };
                            break;
                        case 4:
                            $scope.present = {
                                title: 'Блендер Braun JB 5160 Black',
                                link: 'https://goo.gl/QlqUgc',
                                picture: '/img/gifts/blender.jpg',
                                repost: 'http://womansday.nur.kz/pages/braun-jb-5160.html',
                                text: 'Просто нажимай, детка. Блендер – незаменимая вещь для семейных кулинарных вечеринок.'
                            };
                            break;
                        case 5:
                            $scope.present = {
                                title: 'Щипцы Remington Ci5319 Black-Gold',
                                link: 'https://goo.gl/rvx796',
                                picture: '/img/gifts/remington-gold.jpg',
                                repost: 'http://womansday.nur.kz/pages/remington-gold.html',
                                text: 'Красивые локоны – мечта любой женщины.  Ваше стремление сделать ее еще более красивой будет вознаграждено!'
                            };
                            break;
                    }
                    break;
                case 2:
                    switch ($scope.activeage) {
                        case 2:
                            $scope.present = {
                                title: 'Браслет Xiaomi Mi Band 2 Black',
                                link: 'https://goo.gl/W4lWhI',
                                picture: '/img/gifts/xiamo.jpg',
                                repost: 'http://womansday.nur.kz/pages/xiaomi.html',
                                text: 'Женщине, стремящейся вести активный образ жизни, влагозащищенный фитнес-браслет окажется как нельзя кстати.'
                            };
                            break;
                        case 3:
                            $scope.present = {
                                title: 'Смартфон Samsung Galaxy S7 Edge 32GB Black',
                                link: 'https://goo.gl/VhXtFv',
                                picture: '/img/gifts/samsung-galaxys7.jpg',
                                repost: 'http://womansday.nur.kz/pages/samsung-galaxy-s7-edge-32gb.html',
                                text: 'Духи, конфеты, цветы… Чтобы не ошибиться с выбором, просто подарите ей смартфон.'
                            };
                            break;
                        case 4:
                            $scope.present = {
                                title: 'Посудомоечная машина Gorenje GV50211 White',
                                link: 'https://goo.gl/7FP1dh',
                                picture: '/img/gifts/washer.jpg',
                                repost: 'http://womansday.nur.kz/pages/gorenje.html',
                                text: 'Говорят, если посуду не мыть после еды — рано или поздно придется  ее мыть до еды… Поэтому пусть этим займутся… роботы.'
                            };
                            break;
                        case 5:
                            $scope.present = {
                                title: 'Кухонный комбайн Bosch MUM 4880 Silver',
                                link: 'https://goo.gl/AON2PI',
                                picture: '/img/gifts/bosh.jpg',
                                repost: 'http://womansday.nur.kz/pages/bosch-mum-4880.html',
                                text: 'Поверьте, вы делаете подарок не только ей, но и себе.'
                            };
                            break;
                    }
                    break;
                case 3:
                    switch ($scope.activeage) {
                        case 6:
                            $scope.present = {
                                title: 'Конструктор LEGO Friends 41307 Творческая лаборатория Оливии',
                                link: 'https://goo.gl/7VS86u',
                                picture: '/img/gifts/lego.jpg',
                                repost: 'http://womansday.nur.kz/pages/lego.html',
                                text: 'Кто сказал, что девочки должны учиться быть хорошей хозяйкой? Они тоже могут увлекаться робототехникой. Пусть даже из конструктора. '
                            };
                            break;
                        case 7:
                            $scope.present = {
                                title: 'Мягкая игрушка Chi Chi Love Чихуахуа в розовой футболке с сумочкой и зеркальцем 20 см',
                                link: 'https://goo.gl/Jm1Sr2',
                                picture: '/img/gifts/chichi.jpg',
                                repost: 'http://womansday.nur.kz/pages/chichi.html',
                                text: 'Каждый ребенок хотел бы иметь собственного питомца, но если вы не хотите брать в дом настоящего песика, купите плюшевого. Он не лает, не кусается!'
                            };
                            break;
                        case 1:
                            $scope.present = {
                                title: 'Щипцы Rowenta SF 4522 Black-Silver',
                                link: 'https://goo.gl/sXcAL6',
                                picture: '/img/gifts/rowenta.jpg',
                                repost: 'http://womansday.nur.kz/pages/rowenta-sf-4522.html',
                                text: 'Женщины могут  быть непостоянными. Сегодня мы хотим иметь прямые и гладкие волосы, а завтра - мелкие кудряшки. Без помощников тут не обойтись.'
                            };
                            break;
                        case 2:
                            $scope.present = {
                                title: 'Ноутбук ASUS X540SA-XX012T Brown-Gold',
                                link: 'https://goo.gl/VlKz3H',
                                picture: '/img/gifts/asus.jpg',
                                repost: 'http://womansday.nur.kz/pages/asus-x540sa-xx012t.html',
                                text: 'Многие женщины давно уже не мечтают о парфюмерии или украшениях и хотели бы получить в подарок … ноутбук!'
                            };
                            break;
                        case 3:
                            $scope.present = {
                                title: 'Щипцы Remington S3500 E51 Black',
                                link: 'https://goo.gl/zSBVAk',
                                picture: '/img/gifts/remington-black.jpg',
                                repost: 'http://womansday.nur.kz/pages/remington-black.html',
                                text: 'С помощью щипцом можно не только выпрямлять волосы, но и делать локоны. Так что  с этим подарком ошибки быть не может!'
                            };
                            break;
                        case 4:
                            $scope.present = {
                                title: 'Посудомоечная машина Hotpoint-Ariston LSTF 9M117 C White',
                                link: 'https://goo.gl/mtzume',
                                picture: '/img/gifts/hotpoint.jpg',
                                repost: 'http://womansday.nur.kz/pages/hotpoint.html',
                                text: '"Сегодня 8 марта, не мой сегодня посуду…. Пусть ее помоет посудомоечная машина!" - измените известный анекдот.'
                            };
                            break;
                        case 5:
                            $scope.present = {
                                title: 'Фен Philips HP8230 Black',
                                link: 'https://goo.gl/qv2R7I',
                                picture: '/img/gifts/phillips.jpg',
                                repost: 'http://womansday.nur.kz/pages/philips-hp8230.html',
                                text: 'Фен – один из незатейливых, но всегда нужных подарков. Из-за частоты использования они могут ломаться, поэтому вы не промахнетесь, если выберете этот.'
                            };
                            break;
                    }
                    break;
                case 4:
                    switch ($scope.activeage) {
                        case 6:
                            $scope.present = {
                                title: 'Набор игрушек Markwins Minnie Декоративная косметика для ногтей',
                                link: 'https://goo.gl/VkVe6D',
                                picture: '/img/gifts/nails.jpg',
                                repost: 'http://womansday.nur.kz/pages/markwins.html',
                                text: 'Принцессами не становятся, ими рождаются! А потому с раннего возраста девочки любят наводить красоту, в том числе, и на ногтях.'
                            };
                            break;
                        case 7:
                            $scope.present = {
                                title: 'Мягкая игрушка Orange Медведь Топтыжкин МА1980-30 Yellow',
                                link: 'https://goo.gl/edaC3g',
                                picture: '/img/gifts/mishka.jpg',
                                repost: 'http://womansday.nur.kz/pages/orange.html',
                                text: 'Мягкий мишка – это первый друг для ребенка. С ним приятно и спать, и делиться проблемами в школе.'
                            };
                            break;
                        case 2:
                            $scope.present = {
                                title: 'Планшет Samsung Galaxy Tab E 9.6 SM-T561 8Gb Black',
                                link: 'https://goo.gl/LP19x7',
                                picture: '/img/gifts/samsung-galaxy-tab.jpg',
                                repost: 'http://womansday.nur.kz/pages/samsung-galaxy-gab.html',
                                text: 'Планшет универсален. Читать книги, искать рецепты, написать вам письмо? Все это можно делать почти одновременно.'
                            };
                            break;
                        case 1:
                            $scope.present = {
                                title: 'Наушники Xiaomi In-Ear Headphones Pro Black-Silver',
                                link: 'https://goo.gl/EfR8lT',
                                picture: '/img/gifts/xiamoiair.jpg',
                                repost: 'http://womansday.nur.kz/pages/xiaomi-air.html',
                                text: 'Если сестра ни дня не может прожить без музыки, вы точно попадете в цель'
                            };
                            break;
                        case 3:
                            $scope.present = {
                                title: 'Соковыжималка Polaris PEA 1125 AL Crystal Black',
                                link: 'https://kaspi.kz/shop/p/polaris-pea-1125-al-crystal-black-4900079/',
                                picture: '/img/gifts/polaris.jpg',
                                repost: 'http://womansday.nur.kz/pages/polaris-pea-1125.html',
                                text: 'Для красивой кожи нужно ежедневно выпивать стакан сока в день. Нет, тетра-паки – вчерашний день. Да здравствуют фреши!'
                            };
                            break;
                        case 4:
                            $scope.present = {
                                title: 'Швейная машина Janome My Excel 77/MX 77 White',
                                link: 'https://goo.gl/m3AtUu',
                                picture: '/img/gifts/janome.jpg',
                                repost: 'http://womansday.nur.kz/pages/janome-my-excel-77.html',
                                text: 'Если вам посчастливилось жить рядом с рукодельницей, то вы не представляете, сколько можно денег сэкономить на шоппинге.'
                            };
                            break;
                        case 5:
                            $scope.present = {
                                title: 'Мясорубка Philips HR2721 Black',
                                link: 'https://goo.gl/cYkhY3',
                                picture: '/img/gifts/phillips-myaso.jpg',
                                repost: 'http://womansday.nur.kz/pages/philips-hr2721.html',
                                text: 'Тот самый случай, когда подарок лучше выбирать вместе.'

                            };
                            break;
                    }
                    break;
                case 5:
                    switch ($scope.activeage) {
                        case 6:
                            $scope.present = {
                                title: 'Мягкая игрушка Aurora Лисица Фенек 67-104 White',
                                link: 'https://goo.gl/2AaKGC',
                                picture: '/img/gifts/aurora.jpg',
                                repost: 'http://womansday.nur.kz/pages/aurora.html',
                                text: 'Милая лисичка порадует любую девочку, независимо от возраста.'
                            };
                            break;
                        case 7:
                            $scope.present = {
                                title: 'Кукла Mattel Barbie Модница в клетчатой юбке',
                                link: 'https://goo.gl/VPvbZI',
                                picture: '/img/gifts/barbi.jpg',
                                repost: 'http://womansday.nur.kz/pages/barbie.html',
                                text: 'Барби является мечтой  уже нескольких поколений девочек. '
                            };
                            break;
                        case 1:
                            $scope.present = {
                                title: 'Наушники Kingston HyperX Cloud Core Black',
                                link: 'https://goo.gl/9JWrT0',
                                picture: '/img/gifts/kingston.jpg',
                                repost: 'http://womansday.nur.kz/pages/kingston-hyperx-cloud.html',
                                text: 'Надоели песни, которые она слушает, но она никак не делает звук тише? Подумайте о себе, подарите ей наушники.'
                            };
                            break;
                        case 2:
                            $scope.present = {
                                title: 'Кофеварка REDMOND RCM-1502 Black',
                                link: 'https://goo.gl/qX54Ru',
                                picture: '/img/gifts/redmond-rcm-1520.jpg',
                                repost: 'http://womansday.nur.kz/pages/redmond-rcm-1502.html',
                                text: 'Кофе, может, и не всеисцеляющий эликсир, но очень к этому близок.'
                            };
                            break;
                        case 3:
                            $scope.present = {
                                title: 'Кофеварка Delonghi ESAM 2600 Black',
                                link: 'https://goo.gl/Gg6jpH',
                                picture: '/img/gifts/delonghi.jpg',
                                repost: 'http://womansday.nur.kz/pages/delonghi-esam-2600.html',
                                text: 'Теперь-то она точно пригласит вас на чашечку кофе!'
                            };
                            break;
                        case 4:
                            $scope.present = {
                                title: 'Электрочайник Polaris PWP 3215 Silver',
                                link: 'https://goo.gl/V3DPk5',
                                picture: '/img/gifts/polaris-electro.jpg',
                                repost: 'http://womansday.nur.kz/pages/polaris-pwp-3215.html',
                                text: 'Кто сказал, что чайник – не лучший подарок? Англия, страна, которая ввела моду на чаепитие, сделала трендом и чайник в качестве подарка.'
                            };
                            break;
                        case 5:
                            $scope.present = {
                                title: 'Мультиварка REDMOND RMC-M90 Black',
                                link: 'https://goo.gl/XFEXZP',
                                picture: '/img/gifts/redmond.jpg',
                                repost: 'http://womansday.nur.kz/pages/redmond-rmc-m90.html',
                                text: 'Освободите свою женщину от кухни! Пусть за нее готовит мультиварка.'
                            };
                            break;
                    }
                    break;
                case 6:
                    switch ($scope.activeage) {
                        case 4:
                            $scope.present = {
                                title: 'Планшет Samsung Galaxy Tab E 9.6 SM-T561 8Gb Black',
                                link: 'https://goo.gl/Q1vsfp',
                                picture: '/img/gifts/samsung-galaxy-tab.jpg',
                                repost: 'http://womansday.nur.kz/pages/samsung-galaxy-gab.html',
                                text: 'Планшет универсален. Читать книги, искать рецепты, написать вам письмо? Все это можно делать почти одновременно.'
                            };
                            break;
                        case 5:
                            $scope.present = {
                                title: 'Пароварка Tefal VC 1006 Ultra Compact White',
                                link: 'https://goo.gl/XKO9S9',
                                picture: '/img/gifts/tefal-par.jpg',
                                repost: 'http://womansday.nur.kz/pages/tefal-vc-1006.html',
                                text: 'Она любит готовить, но не любит тратить на это время? А может, сидит на диете и только и мечтает о полезной и вкусной пище? Пароварка ей, ой, вам -  в помощь.'
                            };
                            break;
                    }
                    break;
            }
            if ($scope.activeage && $scope.woman_activeaud)
                $scope.openpresent = true;

        };
        $scope.woman_generator = function () {
            $scope.present = {
                title: false,
                link: '',
                picture: '',
                repost: '',
                text: 'Пожалуйста, выберите другой возраст'
            };
            switch($scope.woman_activeaud) {
                case 1:
                    switch ($scope.activeage) {
                        case 1:
                            $scope.present = {
                                title: 'Смарт-часы Apple Watch Sport 42mm with Sport Band Black',
                                link: 'https://goo.gl/7wKG8V',
                                picture: '/img/gifts/apple-watch.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/apple-watch-sport.html',

                                text: ''
                            };
                            break;
                        case 2:
                            $scope.present = {
                                title: 'Смартфон Apple iPhone 7 32Gb Black',
                                link: 'https://goo.gl/nzlZlQ',
                                picture: '/img/gifts/apple-iphone.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/iphone-7.html',

                                text: ''
                            };
                            break;
                        case 3:
                            $scope.present = {
                                title: 'Планшет Apple iPad Air 2 16Gb Wi-Fi Space Gray',
                                link: 'https://goo.gl/sFJkR1',
                                picture: '/img/gifts/apple-ipad-air.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/ipad-air2.html',

                                text: ''
                            };
                            break;
                        case 4:
                            $scope.present = {
                                title: 'Блендер Braun JB 5160 Black',
                                link: 'https://goo.gl/QlqUgc',
                                picture: '/img/gifts/blender.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/braun-jb-5160.html',

                                text: ''
                            };
                            break;
                        case 5:
                            $scope.present = {
                                title: 'Щипцы Remington Ci5319 Black-Gold',
                                link: 'https://goo.gl/rvx796',
                                picture: '/img/gifts/remington-gold.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/remington-gold.html',

                                text: ''
                            };
                            break;
                    }

                    break;
                case 2:
                    switch ($scope.activeage) {

                        case 2:
                            $scope.present = {
                                title: 'Браслет Xiaomi Mi Band 2 Black',
                                link: 'https://goo.gl/W4lWhI',
                                picture: '/img/gifts/xiamo.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/xiaomi.html',
                                text: ''
                            };
                            break;

                        case 3:
                            $scope.present = {
                                title: 'Смартфон Samsung Galaxy S7 Edge 32GB Black',
                                link: 'https://goo.gl/VhXtFv',
                                picture: '/img/gifts/samsung-galaxys7.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/samsung-galaxy-s7-edge-32gb.html',
                                text: ''
                            };
                            break;
                        case 4:
                            $scope.present = {
                                title: 'Посудомоечная машина Gorenje GV50211 White',
                                link: 'https://goo.gl/7FP1dh',
                                picture: '/img/gifts/washer.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/gorenje2.html',
                                text: ''
                            };
                            break;
                        case 5:
                            $scope.present = {
                                title: 'Кухонный комбайн Bosch MUM 4880 Silver',
                                link: 'https://goo.gl/AON2PI',
                                picture: '/img/gifts/bosh.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/bosch-mum-4880.html',
                                text: ''
                            };
                            break;
                    }
                    break;
                case 3:
                    switch ($scope.activeage) {
                        case 1:
                            $scope.present = {
                                title: 'Наушники Xiaomi In-Ear Headphones Pro Black-Silver',
                                link: 'https://goo.gl/EfR8lT/',
                                picture: '/img/gifts/xiamoiair.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/xiamoiair.html',
                                text: ''
                            };
                            break;
                        case 2:
                            $scope.present = {
                                title: 'Планшет Samsung Galaxy Tab E 9.6 SM-T561 8Gb Black',
                                link: 'https://goo.gl/LP19x7',
                                picture: '/img/gifts/samsung-galaxy-tab.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/samsung-galaxy-gab-2.html',
                                text: ''
                            };
                            break;
                        case 3:
                            $scope.present = {
                                title: 'Соковыжималку Polaris PEA 1125 AL Crystal Black',
                                link: 'https://goo.gl/79yK9y',
                                picture: '/img/gifts/polaris.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/polaris-pea-1125-1.html',

                                text: ''
                            };
                            break;
                        case 4:
                            $scope.present = {
                                title: 'Швейную машину Janome My Excel 77/MX 77 White',
                                link: 'https://goo.gl/m3AtUu',
                                picture: '/img/gifts/janome.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/janome-my-excel-77.html',
                                text: ''
                            };
                            break;
                        case 5:
                            $scope.present = {
                                title: 'Мясорубку Philips HR2721 Black',
                                link: 'https://goo.gl/cYkhY3',
                                picture: '/img/gifts/phillips-myaso.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/philips-hr2721-1.html',

                                text: ''
                            };
                            break;
                    }
                    break;
                case 4:
                    switch ($scope.activeage) {
                        case 1:
                            $scope.present = {
                                title: 'Щипцы Rowenta SF 4522 Black-Silver',
                                link: 'https://goo.gl/sXcAL6',
                                picture: '/img/gifts/rowenta.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/rowenta-sf-4522-son.html',
                                text: ''
                            };
                            break;
                        case 2:
                            $scope.present = {
                                title: 'Ноутбук ASUS X540SA-XX012T Brown-Gold',
                                link: 'https://goo.gl/VlKz3H ',
                                picture: '/img/gifts/asus.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/asus-x540sa-xx012t-1.html',
                                text: ''
                            };
                            break;
                        case 3:
                            $scope.present = {
                                title: 'Щипцы Remington S3500 E51 Black',
                                link: 'https://goo.gl/zSBVAk',
                                picture: '/img/gifts/remington-black.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/remington-black.html',

                                text: ''
                            };
                            break;
                        case 4:
                            $scope.present = {
                                title: 'Посудомоечная машина Hotpoint-Ariston LSTF 9M117 C White',
                                link: 'https://goo.gl/mtzume',
                                picture: '/img/gifts/hotpoint.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/hotpoint.html',

                                text: ''
                            };
                            break;
                        case 5:
                            $scope.present = {
                                title: 'Фен Philips HP8230 Black',
                                link: 'https://goo.gl/qv2R7I',
                                picture: '/img/gifts/phillips.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/philips-hp8230-1.html',

                                text: ''
                            };
                            break;
                    }
                    break;
                    case 5:
                    switch ($scope.activeage) {
                        case 1:
                            $scope.present = {
                                title: 'Наушники Kingston HyperX Cloud Core Black',
                                link: 'https://goo.gl/9JWrT0 ',
                                picture: '/img/gifts/kingston.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/kingston-hyperx-cloud.html',

                                text: ''
                            };
                            break;
                        case 2:
                            $scope.present = {
                                title: 'Кофеварку REDMOND RCM-1502 Black',
                                link: 'https://goo.gl/qX54Ru',
                                picture: '/img/gifts/redmond-rcm-1520.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/redmond-rcm-1502.html',

                                text: ''
                            };
                            break;
                        case 3:
                            $scope.present = {
                                title: 'Кофеварку Delonghi ESAM 2600 Black',
                                link: 'https://goo.gl/Gg6jpH',
                                picture: '/img/gifts/delonghi.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/delonghi-esam-2600.html',

                                text: ''
                            };
                            break;
                        case 4:
                            $scope.present = {
                                title: 'Электрочайник Polaris PWP 3215 Silver',
                                link: 'https://goo.gl/V3DPk5',
                                picture: '/img/gifts/polaris-electro.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/polaris-pwp-3215.html',

                                text: ''
                            };
                            break;
                        case 5:
                            $scope.present = {
                                title: 'Мультиварку REDMOND RMC-M90 Black',
                                link: 'https://goo.gl/XFEXZP',
                                picture: '/img/gifts/redmond.jpg',
                                repost: 'http://womansday.nur.kz/pages/woman/redmond-rmc-m90-1.html',
                                text: 'Освободите свою женщину от кухни! Пусть за нее готовит мультиварка.'
                            };
                            break;

                    }
                    break;
            }
            if ($scope.activeage && $scope.woman_activeaud)
                $scope.openpresent = true;
        }


    }]);


    $('#scene').parallax();

})(jQuery,angular);
